import bpy

export_path = "/home/frankiezafe/projects/tanukis/work_cpp/SoftSkin/bin/data/transi.softskin"

export = open(export_path, "w")
mesh = bpy.data.meshes["transi-export"]
export.write( "++vertices++" + str(len(mesh.vertices)) + "\n" )

i = 0
for v in mesh.vertices:
    export.write( str(i) + " " + str(v.co[0]) + " " + str(v.co[1]) + " " + str(v.co[2]) + " " + str(v.normal[0]) + " " + str(v.normal[1]) + " " + str(v.normal[2]) + "\n" )
    export.flush()
    i += 1

export.write( "++edges++" + str(len(mesh.edges)) + "\n" )
for e in mesh.edges:
    s = "0"
    if e.use_seam == True:
        s = "1"
    export.write( str(e.vertices[0]) + " " + str(e.vertices[1]) + " " + s + "\n" )
    export.flush()
    
export.write( "++faces++" + str(len(mesh.polygons)) + "\n" )
for p in mesh.polygons:
    for i in range(0,len(p.vertices)):
        if i > 0:
            export.write( " " )
        export.write( str(p.vertices[i]) )
    export.write( "\n" )
    export.flush()
    
export.close()
print( mesh, len(mesh.vertices) )
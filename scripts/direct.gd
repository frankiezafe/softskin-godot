extends ImmediateGeometry

var total_time = 0

var uvs = []
var normals = []
var vecs = []
var faces = []
var l = 3

var parse_vec = false
var parse_edges = false
var parse_faces = false

func parse_softskin():
	var file = File.new()
	file.open("res://data/transi.softskin", file.READ)
#	var content = file.get_as_text()
	var l = file.get_line()
	while! l.empty():
		parse_softline( l )
		l = file.get_line()
	file.close()
	
	print( vecs.size() )
	print( faces.size() )
	
func parse_softline( l ):
	if l.begins_with( "++vertices++" ):
		parse_vec = true
		parse_edges = false
		parse_faces = false
		return
	elif l.begins_with( "++edges++" ):
		parse_vec = false
		parse_edges = true
		parse_faces = false
		return
	elif l.begins_with( "++faces++" ):
		parse_vec = false
		parse_edges = false
		parse_faces = true
		return
	if parse_vec:
		var data = l.split_floats( " " )
		if data.size() == 7:
			vecs.append( Vector3( data[1], data[2], data[3] )  )
			normals.append( Vector3( data[4], data[5], data[6] )  )
	elif parse_faces:
		var data = l.split_floats( " " )
		if data.size() == 3:
			faces.append( [ int(data[0]), int(data[1]), int(data[2]) ] )

func _process(delta):
	
	total_time += delta
	
	clear()
	begin(Mesh.PRIMITIVE_TRIANGLES)
	for f in faces:
		set_normal( normals[f[2]] )
		add_vertex( vecs[f[2]] )
		set_normal( normals[f[1]] )
		add_vertex( vecs[f[1]] )
		set_normal( normals[f[0]] )
		add_vertex( vecs[f[0]] )
	end()
	
	rotate_y( delta * 0.2 )

func _ready():
	
	parse_softskin()
	
	uvs.append( Vector2( 0, 0 ) )
	vecs.append( Vector3( -1, 0, 0 ) )
	uvs.append( Vector2( 1, 1 ) )
	vecs.append( Vector3( 0, 0, -1 ) )
	uvs.append( Vector2( 0, 1 ) )
	vecs.append( Vector3( 0, 0, 1 ) )
	
	pass
